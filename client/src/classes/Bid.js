import { resolveTransitionHooks } from "vue";

class Bid {
	constructor(object) {
		this.id = object.id;
		this.bid = object.bid;
		this.sequence = object.sequence;
		this.description = object.description;
		this.group = object.group;
		this.addedBy = object.addedBy;
		this.addedAt = object.addedAt;
		this.keywords = object.keywords;
	}
	update(description) {
		this.description = description;
		// fetch
	}
}

export default Group;