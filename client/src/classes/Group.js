class Group {
	constructor(object) {
		this.id = object.id;
		this.name = object.name;
		this.users = object.users;
		this.sharecodes = object.sharecodes;
		this.createdBy = object.createdBy;
		this.createdAt = object.createdAt;
	}
	addUser() {

	}
	generateShareCode() {
		
	}
	generateShareLink() {

	}
	delete() {
		
	}
}

export default Group;