class User {
	constructor(object) {
		this.id = object.id;
		this.username = object.username;
		this.cezar = object.cezar;
		this.token = object.token;
		this.createdAt = object.createdAt;
	}
	getToken() {
		return this.token;
	}
	setToken(token) {
		this.token = token;
	}
	changePassword() {

	}
	changeUsername() {

	}
	delete() {

	}
}

export default User;