import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'

import 'materialize-css/dist/css/materialize.min.css'
import 'material-design-icons/iconfont/material-icons.css'
import './styles/main.scss';

let app = createApp(App)

app.directive('click-outside-bidding', {
	beforeMount(el, binding) {
		el.clickOutsideEvent = (event) => {
			if (!(el === event.target || el.contains(event.target)) && !(event.target.classList.contains('bid') || event.target.parentNode.classList.contains('bid')) ) {
				binding.value(event, el);
			}
		};
		document.body.addEventListener('click', el.clickOutsideEvent);
	},
	unmounted(el) {
		document.body.removeEventListener('click', el.clickOutsideEvent);
	}
});

app.use(router);
app.use(store);
app.mount('#app');
