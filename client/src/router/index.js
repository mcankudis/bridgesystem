import {
	createRouter,
	createWebHistory
} from 'vue-router';
// import store from '../store';
// import { users } from "../assets/users";
import Home from "../views/Home";
import User from "../views/User";
import Groups from "../views/Groups";
import System from "../views/System";
import Bidding from "../views/Bidding";
import Login from "../views/Login";
import Register from "../views/Register";

const routes = [{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/system',
		name: 'System',
		component: System
	},
	{
		path: '/bidding',
		name: 'Bidding',
		component: Bidding
	},
	{
		path: '/profile',
		name: 'Profile',
		component: User
	},
	{
		path: '/groups',
		name: 'Groups',
		component: Groups
	},
	{
		path: '/user/:userId',
		name: 'UserProfile',
		component: User
	},
	{
		path: '/register',
		name: 'Register',
		component: Register
	},
	{
		path: '/login',
		name: 'Login',
		component: Login
	},
	//   {
	//     path: '/admin',
	//     name: 'Admin',
	//     component: Admin,
	//     meta: {
	//       requiresAdmin: true
	//     }
	//   }
]

const router = createRouter({
	history: createWebHistory(),
	routes
})

router.beforeEach(async (to, from, next) => {
	//   const user = store.state.User.user;

	//   if (!user) {
	//     await store.dispatch('User/setUser', users[0]);
	//   }

	//   const isAdmin = false;
	//   const requiresAdmin = to.matched.some(record => record.meta.requiresAdmin);

	//   if (requiresAdmin && !isAdmin) next({ name: 'Home' });
	//   else next();
	next();
})

export default router