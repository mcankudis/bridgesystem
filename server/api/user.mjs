"use strict";
import express from 'express';
import jwt from "jsonwebtoken";
import User from '../models/User.mjs';
import {verifyString, verifyEmail, verifyNumber} from "../functions/sanitize.mjs";
import getRandomString from "../functions/getRandomString.mjs"
import { comparePassword, hashPassword } from '../functions/crypto.mjs';
// import ensureAuthenticated from "../functions/ensureAuthenticated.mjs"

const router = express.Router();

router.post('/register', async(req, res) => {
	try {
		if(!verifyString(req.body.username)) throw {code: "", msg: "Invalid username"};
		if(!verifyString(req.body.password)) throw {code: "", msg: "Invalid password"};
		if(!verifyEmail(req.body.email)) throw {code: "", msg: "Invalid email"};
		if(req.body.cezar && !verifyNumber(req.body.cezar)) throw {code: "", msg: "Invalid cezar"};
		let hash = hashPassword(req.body.password);
		let user = new User({
			username: req.body.username,
			password: hash,
			email: req.body.email,
			cezar: req.body.cezar,
			reissue_id: getRandomString(40, 60)
		});
		await user.save();
		res.json(user);
	}
	catch(err) {
		console.error(`[${new Date}, POST /user/register]`, err);
		if(err.code === 11000) {
			if(err.message.includes('email')) return res.status(400).send("Email already in use");
			if(err.message.includes('cezar')) return res.status(400).send("Cezar already in use");
			return res.status(400).send("Username already in use");
		}
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.post('/login', async(req, res) => {
	try {
		if(!verifyString(req.body.username)) throw {code: "", msg: "Invalid username"};
		if(!verifyString(req.body.password)) throw {code: "", msg: "Invalid password"};
		let user = await User.getByUsername(req.body.username);
		if(!user) throw {code: "", msg: "User not found"}
		let validPassword = await comparePassword(req.body.password, user.password);
		if(!validPassword) throw {code: "", msg: "Invalid password"}
		let random = getRandomString(40, 60);
		let token = jwt.sign({
			username: user.username,
			id: user.id,
			random: random
		}, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});
		random = getRandomString(40, 60);
		let reissueToken = jwt.sign({
			id: user.id,
			random: random,
			reissue_id: user.reissue_id
		}, process.env.REISSUE_SECRET, {algorithm: 'HS512', expiresIn: '7d'});
		res.json({
			token,
			reissueToken,
			user: {
				username: user.username,
				id: user.id
			}
		});
	}
	catch(err) {
		console.error(`[${new Date}, /user/login]`, err);
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.get('/token/reissue', async(req, res) => {
	if(!verifyToken(req.headers['x-reissue'])) return res.status(400).send("Invalid authorization data");
	if(!req.headers['x-reissue']) return res.status(400).send("Invalid authorization data");
	try {
		let tokenData = jwt.verify(req.headers['x-reissue'], process.env.REISSUE_SECRET, {algorithms: ['HS512']});
		if(typeof tokenData === 'string') throw {code: 403, msg: "Missing credentials"};
		let user = await User.getById(tokenData.id);
		if(!user) throw {code: 404, msg: "Account not found"};
		if(tokenData.reissue_id!==user.reissue_id) return res.status(400).send("Invalid authorization data");
		let random = getRandomString(40, 60);
		let token = jwt.sign({
			username: user.username,
			id: user.id,
			random: random
		}, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});
		random = getRandomString(40, 60);
		let reissueToken = jwt.sign({
			id: user.id,
			random: random,
			reissue_id: user.reissue_id
		}, process.env.REISSUE_SECRET, {algorithm: 'HS512', expiresIn: '7d'});
		res.json({token, reissueToken});
	}
	catch(err) {
		console.error(`[${new Date}, /user/token/reissue]`, err);
		if(err.msg) return res.status(err.code).send(err.msg);
		res.sendStatus(500);
	}
})

export default router;