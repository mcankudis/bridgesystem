"use strict";
import mongoose from 'mongoose';
const dbpath = process.env.DB_PATH || 'mongodb://localhost/bridgesystem';

mongoose.connect(dbpath, {
    user: process.env.DB_USER,
    pass: process.env.DB_PASS,
    useNewUrlParser: true,
    useCreateIndex: true,
	useFindAndModify: true,
	useUnifiedTopology: true
}, err => {
  if (err) console.error(`[${new Date}, db/connect]`, err);
  else console.log(`[${new Date}, db/connect]`, `Connected with the DB`);
});

export default mongoose