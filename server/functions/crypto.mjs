"use strict";
import bcrypt from "bcryptjs";

export const comparePassword = async(password, userPassword) => {
	try {
		const isMatch = await bcrypt.compare(password, userPassword);
		if(!isMatch) return false;
		return true;
	}
	catch(err) {
		console.error(`[${new Date}, functions/comparePassword]`, err);
		throw err;
	}
}

export const hashPassword = (password) => {
	try {
		let salt = bcrypt.genSaltSync(10);
		let hash = bcrypt.hashSync(password, salt);
		return hash;
	}
	catch(err) {
		console.error(`[${new Date}, functions/hashPassword]`, err);
		throw err;
	}

};