"use strict";

const loger = (req, res, next) => {
	console.log(new Date(), req.ip, req.originalUrl);
    return next();
}

export default loger;