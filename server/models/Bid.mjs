var db = require('../db')

const bidsSchema = new db.Schema({
	bid: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	sequence: {
		type: Array,
		required: true
	},
	keywords: {
		type: Array,
		required: false
	},
	group: {
		type: String,
		required: true
	},
	addedAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	addedBy: {
		type: String,
		required: true
	}
});

bidsSchema.methods.add = async (bid) => {
	try {
		// validation
		let newBid = new Bid({
			bid: bid.bid,
			description: bid.description,
			sequence: bid.sequence,
			keywords: bid.keywords || [],
			group: bid.group,
			addedAt: Date.now(),
			addedBy: bid.addedBy
		})
		newBid.save();
		return newBid;
	} catch (err) {
		console.error(`[${new Date}, Bid.add]`, err);
		throw err;
	}
}

const Bid = db.model('bid', bidsSchema);
export default Bid;