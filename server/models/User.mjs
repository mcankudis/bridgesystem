import db from "../db.mjs"

// Schema
const UserSchema = db.Schema({
	username: {
		type: String,
		unique: true,
		required: true,
	},
	password: {
		type: String,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	cezar: {
		type: Number,
		required: false
	},
	reissue_id: {
		type: String,
		required: true
	}
}, {
	timestamps: true
})

// Static methods
UserSchema.statics.getById = async(id) => {
	try {
		let user = await this.findById(id).exec();
		return user;
	}
	catch(err) {
		console.error(`[${new Date}, User/findById]`, err);
		throw err;
	}
}
UserSchema.statics.getByUsername = async(username) => {
	try {
		let user = await User.findOne({username: username}).exec();
		return user;
	}
	catch(err) {
		console.error(`[${new Date}, User/findByUsername]`, err);
		throw err;
	}
}

const User = db.model("User", UserSchema);
export default User;