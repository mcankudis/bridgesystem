'use strict';

import './env.mjs';
import express from 'express';
import bodyparser from 'body-parser';
import helmet from 'helmet';

import loger from './middleware/loger.mjs';
import user from './api/user.mjs';

const app = express();
app.use(helmet());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));

// import team from './api/team.mjs';
// import player from './api/player.mjs';
// import match from './api/match.mjs';
app.use('/user', loger, user);
// app.use('/team', loger, team);
// app.use('/player', loger, player);
// app.use('/match', loger, match);

app.get('/', (req, res) => {
	res.send('hello');
})

const port = process.env.PORT || 3267;
app.listen(port, () => {
	console.log(new Date, `Hi, listening on port ${port}`);
})

export default app;